﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Accord.MachineLearning.DecisionTrees;
using Accord.Statistics.Filters;
using DataMiningDecisionTree_WPF.Controller;
using DataMiningDecisionTree_WPF.Model;
using System.Collections.Generic;
using Accord.MachineLearning.DecisionTrees.Learning;
using Accord.Math;
using System.IO;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup.Localizer;
using System.Windows.Media;

namespace DataMiningDecisionTree_WPF.View
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }


        string s_DBPath = "";
        string s_testDBPath = "";

        List<List<string>> lls_trainingSet;
        List<List<string>> lls_trainingSetTranspose;
        List<List<string>> lls_testSet;

        ObservableCollection<AttributeModel> oc_IncludeAttributeList = new ObservableCollection<AttributeModel>();
        ObservableCollection<AttributeModel> oc_ExcludeAttributeList = new ObservableCollection<AttributeModel>();
        public AttributeModel am_SetAsClasses = new AttributeModel();

        Task t_TaskMining;
        CancellationTokenSource cts_TaskCancleToken;


        DateTime dt_Start;
        DateTime dt_End;
        DateTime dt_Mid;
        TimeSpan ts_BuildModel;
        TimeSpan ts_TestingModel;
        TimeSpan ts_diff;

        ////////// new
        private DataTable _originDataTable;

        private DataTable _testDataTable;

        private DecisionTree _decisionTree;
        private Codification _codeBook;


        async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            lv_IncludeAttribute.ItemsSource = oc_IncludeAttributeList;
            lv_ExcludeAttribute.ItemsSource = oc_ExcludeAttributeList;

            //自動化程序
            //s_DBPath = @"e:\Users\Nathaniel\Desktop\customer_card_result_training.txt";
            //tb_dbPath.Text = s_DBPath;
            //await parseDB();

            ////自動化程序
            //s_testDBPath = @"e:\Users\Nathaniel\Desktop\customer_card_result_test.txt";
            //tb_testdbPath.Text = s_testDBPath;
            //await parseTestDB();

            //btn_execute_Click(null, null);
        }



        private async void btn_opendb_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".txt",
                Filter = "Text documents (.txt)|*.txt"
            };


            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                s_DBPath = dlg.FileName;
                tb_dbPath.Text = dlg.FileName;

                parseDB();


            }


        }

        private void parseDB()
        {
            ParseTXT parseTxt = new ParseTXT();
            _originDataTable = parseTxt.TransformToDataTable(s_DBPath);


            int tempColumn = 0;
            oc_IncludeAttributeList.Clear();
            foreach (DataColumn Attribute in _originDataTable.Columns)
            {
                oc_IncludeAttributeList.Add(new AttributeModel() { Column = tempColumn, AttributeName = Attribute.Caption });
                tempColumn++;
            }
        }


        private void parseTestDB()
        {
            ParseTXT parseTxt = new ParseTXT();
            _testDataTable = parseTxt.TransformToDataTable(s_testDBPath);

        }


        private async void btn_execute_Click(object sender, RoutedEventArgs e)
        {
            if (isClassesSelected)
            {
                rtb_main.AppendText(String.Format("{0}{1}", "", Environment.NewLine));
                rtb_main.AppendText(String.Format("{0}{1}", ">>>>Start!<<<<", Environment.NewLine));

                //Task的Cancel Token
                cts_TaskCancleToken = new CancellationTokenSource();

                string ClassesColName = am_SetAsClasses.AttributeName;

                DataTable filtedTable = SelectDataColumns(_originDataTable, oc_ExcludeAttributeList.ToList());


                if (cb_OneDB.IsChecked == true)
                {
                    List<int> filterAttributeValuesCount = FindHowManyValuePerAttribute(filtedTable);
                    //切切樂(X)，分training set或test set(O)
                    List<DataTable> dts = SetDivide(filtedTable, Convert.ToDouble(tb_trainingRatio.Text), Convert.ToDouble(tb_testingRatio.Text));


                    dt_Start = DateTime.Now;

                    //dts[0] = training set, [1] = testing set  //filtedTable輸入當codex的learing set(為確保model裡面也有編到testing set的value)
                    BuildingID3Model(dts[0], filterAttributeValuesCount, filtedTable);

                    dt_Mid = DateTime.Now;

                    TestModel(dts[1]);

                    dt_End = DateTime.Now;
                    ts_BuildModel = dt_Mid - dt_Start;
                    ts_TestingModel = dt_End - dt_Mid;
                    ts_diff = dt_End - dt_Start;
                }
                else
                {
                    DataTable filtedTable_testSet = SelectDataColumns(_testDataTable, oc_ExcludeAttributeList.ToList());

                    datagrid_training.ItemsSource = filtedTable.AsDataView();
                    datagrid_testing.ItemsSource = filtedTable_testSet.AsDataView();


                    //在Build Model的時候必須要連test set的value都一起看，才能編出個屬性所有value的codex
                    DataTable LearningCodexTable = new DataTable();
                    LearningCodexTable = filtedTable.Copy();

                    foreach (DataRow dr in filtedTable_testSet.Rows)
                    {
                        LearningCodexTable.Rows.Add(dr.ItemArray);
                    }

                    List<int> filterAttributeValuesCount = FindHowManyValuePerAttribute(LearningCodexTable);


                    dt_Start = DateTime.Now;

                    BuildingID3Model(filtedTable, filterAttributeValuesCount, LearningCodexTable);

                    dt_Mid = DateTime.Now;

                    TestModel(filtedTable_testSet);


                    dt_End = DateTime.Now;
                    ts_BuildModel = dt_Mid - dt_Start;
                    ts_TestingModel = dt_End - dt_Mid;
                    ts_diff = dt_End - dt_Start;
                }

                this.tb_status.Text = "End task.";
                rtb_main.AppendText(String.Format("{0}{1}", "", Environment.NewLine));
                rtb_main.AppendText(String.Format("{0}{1}", "Building Model Time:" + ts_BuildModel.ToString(),
                    Environment.NewLine));
                rtb_main.AppendText(String.Format("{0}{1}", "Testing Model Time:" + ts_TestingModel.ToString(),
                    Environment.NewLine));
                rtb_main.AppendText(String.Format("{0}{1}", "Total Execute Time:" + ts_diff.ToString(),
                    Environment.NewLine));

                rtb_main.AppendText(String.Format("{0}{1}", ">>>>Finished!<<<<", Environment.NewLine));

                tabControl1_SelectionChanged(null, null);

                ////非同步Dispatcher
                //t_TaskMining = new Task(() =>
                //{

                //    Dispatcher.BeginInvoke(new Action(() =>
                //    {
                //        pb_main.IsIndeterminate = true;
                //        this.tb_status.Text = "Count HowManyValuePerAttribute...";
                //    }));



                //    //開始計時
                //    dt_Start = DateTime.Now;


                //    /////blahblahblah




                //}, cts_TaskCancleToken.Token);


                //// t_TaskMining.Start();

                ////結束收尾印東西
                //t_TaskMining.ContinueWith((task) =>
                //{
                //    dt_End = DateTime.Now;
                //    ts_diff = dt_End - dt_Start;

                //    Dispatcher.BeginInvoke(new Action(() =>
                //    {
                //        pb_main.IsIndeterminate = false;
                //        this.tb_status.Text = "End task.";
                //        rtb_main.AppendText(String.Format("{0}{1}", "", Environment.NewLine));
                //        rtb_main.AppendText(String.Format("{0}{1}", "Execute Time:" + ts_diff.ToString(),
                //            Environment.NewLine));

                //        rtb_main.AppendText(String.Format("{0}{1}", ">>>>Finished!<<<<", Environment.NewLine));



                //    }));
                //    Console.WriteLine("Task ended. Task return status:");
                //    Console.WriteLine("IsCanceled={0}\tIsCompleted={1}\tIsFaulted={2}", task.IsCanceled,
                //        task.IsCompleted, task.IsFaulted);
                //});

            }
            else
            {
                MessageBox.Show("Please select classes!");
            }

        }

        //分割training set跟test set
        private List<DataTable> SetDivide(DataTable filtedTable, double trainingRatio, double testingRatio)
        {

            DataView dv = filtedTable.DefaultView;

            dv.Sort = am_SetAsClasses.AttributeName;

            DataTable sortedByClassesTable = dv.ToTable();


            datagrid_main.ItemsSource = sortedByClassesTable.AsDataView();

            DataTable dataTables_trainingSet = new DataTable();
            DataTable dataTables_testingSet = new DataTable();

            //複製結構但不複製資料
            dataTables_trainingSet = filtedTable.Clone();
            dataTables_testingSet = filtedTable.Clone();

            foreach (String attribute in am_SetAsClasses.AttributeValues)
            {
                DataTable tempDataTable = new DataTable("Training Set - " + attribute);

                //這兩個方式一樣，但後來還是拿table，留著參考
                //var selectedSet = sortedByClassesTable.Select(am_SetAsClasses.AttributeName + "='" + attribute + "'");

                //DataRow[] selectedSet = (from DataRow dr in sortedByClassesTable.Rows
                //                    where dr[am_SetAsClasses.AttributeName].Equals(attribute)
                //                    select dr).CopyToDataTable().Select();

                DataTable selectedSet = (from DataRow dr in sortedByClassesTable.Rows
                                         where dr[am_SetAsClasses.AttributeName].Equals(attribute)
                                         select dr).CopyToDataTable();

                DataTable selectedSet_random = CollectionExtensions.OrderRandomly(selectedSet.AsEnumerable()).CopyToDataTable();
                datagrid_main2.ItemsSource = selectedSet_random.AsDataView();

                int rowCount = selectedSet_random.Rows.Count;

                Double trainingSetCount = rowCount * trainingRatio;

                DataRow[] selectedSet_Training = (selectedSet_random.Select().Take((int)trainingSetCount)).CopyToDataTable().Select();
                DataRow[] selectedSet_Testing = (selectedSet_random.Select().Skip((int)trainingSetCount)).Take(rowCount -
                    (int)trainingSetCount).CopyToDataTable().Select();

                foreach (DataRow row in selectedSet_Training)
                {
                    dataTables_trainingSet.Rows.Add(row.ItemArray);
                }

                foreach (DataRow row in selectedSet_Testing)
                {
                    dataTables_testingSet.Rows.Add(row.ItemArray);
                }
            }

            //輸出到dataview視覺化
            datagrid_training.ItemsSource = dataTables_trainingSet.AsDataView();
            datagrid_testing.ItemsSource = dataTables_testingSet.AsDataView();

            List<DataTable> returDataTables = new List<DataTable>();
            returDataTables.Add(dataTables_trainingSet);
            returDataTables.Add(dataTables_testingSet);

            return (returDataTables);

        }

        private List<int> FindHowManyValuePerAttribute(DataTable filtedTable)
        {
            List<List<string>> AttributeValueCount_temp = new List<List<string>>();

            List<int> AttributeValueCount = new List<int>();

            foreach (DataColumn attributeName in filtedTable.Columns)
            {
                List<string> colValueCount = (from DataRow dr in filtedTable.Rows
                                              select dr[attributeName.Caption]).Distinct().OfType<string>().ToList();

                AttributeValueCount_temp.Add(colValueCount);
            }

            //順便找classes的value有哪幾種
            am_SetAsClasses.AttributeValues = AttributeValueCount_temp[AttributeValueCount_temp.Count - 1];


            foreach (List<string> AttributeValues in AttributeValueCount_temp)
            {
                AttributeValueCount.Add(AttributeValues.Count);
            }

            //統計出每個Attribute有多少個Value
            return AttributeValueCount;
        }

        private void BuildingID3Model(DataTable inpuTable, List<int> filterAttributeValuesCount, DataTable LearningCodexTable)
        {
            //  Codification codebook = new Codification(inpuTable);
            Codification codebook = new Codification(LearningCodexTable);

            List<DecisionVariable> attributes = new List<DecisionVariable>();
            int i = 0;

            foreach (DataColumn attributeName in inpuTable.Columns)
            {
                attributes.Add(new DecisionVariable(attributeName.Caption, filterAttributeValuesCount[i]));
                i++;
            }

            List<string> dataColumnsName = (from c in inpuTable.Columns.Cast<DataColumn>()
                                            select c.Caption).ToList();

            // 在 SelectDataColumns已經有把classes Attribute移到table最後一個col了
            //int ClassesColumnIndex = dataColumnsName.FindIndex(x => x.Equals(am_SetAsClasses.AttributeName));

            am_SetAsClasses.FilterColumn = inpuTable.Columns.Count - 1;

            // 找出classes的value有哪幾種(例如信用卡有4種)
            int classCount = filterAttributeValuesCount[am_SetAsClasses.FilterColumn];

            //把要輸入的屬性清單刪掉TARGET ATTRIBUTE
            List<DecisionVariable> attributesWithoutClassesAttribute = new List<DecisionVariable>(attributes);
            attributesWithoutClassesAttribute.RemoveAt(am_SetAsClasses.FilterColumn);

            DecisionTree tree = new DecisionTree(attributesWithoutClassesAttribute, classCount);

            // Create a new instance of the ID3 algorithm
            ID3Learning id3learning = new ID3Learning(tree);

            // Translate our training data into integer symbols using our codebook:
            DataTable symbols = codebook.Apply(LearningCodexTable);


            //把要輸入的屬性清單刪掉TARGET ATTRIBUTE
            List<string> dataColumnsNameWithoutClassesAttribute = new List<string>(dataColumnsName);
            dataColumnsNameWithoutClassesAttribute.RemoveAt(am_SetAsClasses.FilterColumn);


            int[][] inputs = symbols.ToIntArray(dataColumnsNameWithoutClassesAttribute.ToArray());
            int[] outputs = symbols.ToIntArray(am_SetAsClasses.AttributeName).GetColumn(0);

            // Learn the training instances!
            id3learning.Run(inputs, outputs);
            _decisionTree = tree;
            _codeBook = codebook;


            //int[] query = codebook.Translate("S", "F", "4", "1", "Partial College","Gold", "39", "100000");
            //int[] query = codebook.Translate("S", "F", "4", "1", "Partial College", "39", "100000");
            //int output = tree.Compute(query);

            //string answer = codebook.Translate(am_SetAsClasses.AttributeName, output); // answer  


        }

        private void TestModel(DataTable filtedTable_testSet)
        {
            List<AttributeValueCountModel> ACMs_real = new List<AttributeValueCountModel>();
            List<AttributeValueCountModel> ACMs_test = new List<AttributeValueCountModel>();
            foreach (string attributeValueName in am_SetAsClasses.AttributeValues)
            {
                ACMs_real.Add(new AttributeValueCountModel() { AttributeValueName = attributeValueName, Count = 0 });
                ACMs_test.Add(new AttributeValueCountModel() { AttributeValueName = attributeValueName, Count = 0 });
            }

            DataTable filtedTable_testSet_with_result = filtedTable_testSet.Copy();
            filtedTable_testSet_with_result.Columns.Add(am_SetAsClasses.AttributeName + "_RESULT", typeof(string));
            filtedTable_testSet_with_result.Columns.Add("COMPARE_RESULT", typeof(string));
            //統計真實的數量
            foreach (DataRow dr in filtedTable_testSet.Rows)
            {

                string answer = dr.ItemArray[am_SetAsClasses.FilterColumn].ToString();

                AttributeValueCountModel addCountValue = ACMs_real.First(x => x.AttributeValueName == answer);
                addCountValue.Count++;

            }


            int resultColunmCount = filtedTable_testSet_with_result.Columns.Count;
            int z = 0;
            List<string> predictAnswers = new List<string>();
            //經過model test後的數量
            foreach (DataRow dr in filtedTable_testSet.Rows)
            {
                int[] query = _codeBook.Translate(dr.ItemArray.OfType<string>().ToArray());
                int output = _decisionTree.Compute(query);

                string answer = _codeBook.Translate(am_SetAsClasses.AttributeName, output);

                AttributeValueCountModel AttrubyteValueModel = ACMs_test.First(x => x.AttributeValueName == answer);
                AttrubyteValueModel.Count++;



                filtedTable_testSet_with_result.Rows[z][resultColunmCount - 2] = answer;

                if (filtedTable_testSet_with_result.Rows[z][resultColunmCount - 3].Equals(answer))
                {
                    filtedTable_testSet_with_result.Rows[z][resultColunmCount - 1] = "TRUE";

                    //TP
                    AttrubyteValueModel.TP_Count++;

                }
                else
                {
                    filtedTable_testSet_with_result.Rows[z][resultColunmCount - 1] = "FALSE";


                    AttributeValueCountModel AttrubyteValueModel_FN =
                        ACMs_test.First(x => x.AttributeValueName.Equals(filtedTable_testSet_with_result.Rows[z][resultColunmCount - 3]));
                    AttributeValueCountModel AttrubyteValueModel_FP =
                       ACMs_test.First(x => x.AttributeValueName.Equals(filtedTable_testSet_with_result.Rows[z][resultColunmCount - 2]));

                    //FN
                    AttrubyteValueModel_FN.FN_Count++;


                    //FP
                    AttrubyteValueModel_FP.FP_Count++;
                }



                predictAnswers.Add(answer);

                z++;

            }


            if (cb_PredictResult.IsChecked == true)
            {
                File.WriteAllLines("D:\\predictAnswers.txt", predictAnswers);
            }


            datagrid_result.ItemsSource = filtedTable_testSet_with_result.AsDataView();


            int TotalTP = 0;
            foreach (AttributeValueCountModel acm in ACMs_test)
            {
                TotalTP += acm.TP_Count;

                double Percision = Convert.ToDouble(acm.TP_Count) / Convert.ToDouble(acm.TP_Count + acm.FP_Count);
                double Recall = Convert.ToDouble(acm.TP_Count) / Convert.ToDouble(acm.TP_Count + acm.FN_Count);

                rtb_main.AppendText(String.Format
                    ("{0}{1}", "Attribute:[" + acm.AttributeValueName + "] Percision:" + Percision + " Recall:" + Recall,
                    Environment.NewLine));

            }

            double Accuarcy = Convert.ToDouble(TotalTP) / Convert.ToDouble(filtedTable_testSet.Rows.Count);

            rtb_main.AppendText(String.Format
                 ("{0}{1}", "Accuarcy:" + Accuarcy, Environment.NewLine));

        }

        private DataTable SelectDataColumns(DataTable inputTable, List<AttributeModel> selectedColumnsName)
        {
            DataTable outputTable = inputTable.Copy();
            foreach (AttributeModel attribute in selectedColumnsName)
            {
                outputTable.Columns.Remove(attribute.AttributeName);
            }

            //sort columns, 把指定的classes columns移到最後一個columns(因為後面build mode的codex要看順序)
            outputTable.Columns[am_SetAsClasses.AttributeName].SetOrdinal(outputTable.Columns.Count - 1);

            return outputTable;
        }

        private void btn_stop_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_MoveRight_Click(object sender, RoutedEventArgs e)
        {
            if (lv_IncludeAttribute.SelectedItem != null)
            {
                oc_ExcludeAttributeList.Add((AttributeModel)lv_IncludeAttribute.SelectedItem);
                oc_IncludeAttributeList.Remove((AttributeModel)lv_IncludeAttribute.SelectedItem);
                oc_ExcludeAttributeList = new ObservableCollection<AttributeModel>(oc_ExcludeAttributeList.OrderBy(x => x.Column));

                lv_ExcludeAttribute.ItemsSource = oc_ExcludeAttributeList;
            }
        }

        private void btn_MoveLeft_Click(object sender, RoutedEventArgs e)
        {
            if (lv_ExcludeAttribute.SelectedItem != null)
            {
                oc_IncludeAttributeList.Add((AttributeModel)lv_ExcludeAttribute.SelectedItem);
                oc_ExcludeAttributeList.Remove((AttributeModel)lv_ExcludeAttribute.SelectedItem);
                oc_IncludeAttributeList = new ObservableCollection<AttributeModel>(oc_IncludeAttributeList.OrderBy(x => x.Column));

                lv_IncludeAttribute.ItemsSource = oc_IncludeAttributeList;
            }

        }

        private bool isClassesSelected = false;

        private void btn_SetAsClasses_Click(object sender, RoutedEventArgs e)
        {

            rtb_main.Document.Blocks.Clear();

            if (lv_IncludeAttribute.SelectedItem != null)
            {
                am_SetAsClasses = (AttributeModel)lv_IncludeAttribute.SelectedItem;
                rtb_main.AppendText(String.Format("{0}{1}", "Set " + am_SetAsClasses.AttributeName + " as Classes",
                    Environment.NewLine));


                isClassesSelected = true;
            }
            else
            {
                MessageBox.Show("Please select a attribute!");
            }


        }

        private async void btn_opendb_test_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".txt",
                Filter = "Text documents (.txt)|*.txt"
            };


            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                s_testDBPath = dlg.FileName;
                tb_testdbPath.Text = dlg.FileName;

                parseTestDB();


            }


        }


        private void cb_OneDB_Click(object sender, RoutedEventArgs e)
        {
            if (cb_OneDB.IsChecked == false)
            {

                btn_opendb_test.IsEnabled = true;
                tb_trainingRatio.IsEnabled = false;
                tb_testingRatio.IsEnabled = false;
            }
            else
            {
                btn_opendb_test.IsEnabled = false;
                tb_trainingRatio.IsEnabled = true;
                tb_testingRatio.IsEnabled = true;

            }

        }

        private void tabControl1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string row = "0";
            string col = "0";
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    row = datagrid_result.Items.Count.ToString();
                    col = datagrid_result.Columns.Count.ToString();
                    break;

                case 1:
                    row = datagrid_training.Items.Count.ToString();
                    col = datagrid_training.Columns.Count.ToString();
                    break;

                case 2:
                    row = datagrid_testing.Items.Count.ToString();
                    col = datagrid_testing.Columns.Count.ToString();
                    break;

                case 3:
                    row = datagrid_main.Items.Count.ToString();
                    col = datagrid_main.Columns.Count.ToString();
                    break;

                case 4:
                    row = datagrid_main2.Items.Count.ToString();
                    col = datagrid_main2.Columns.Count.ToString();
                    break;

            }


            tb_rowCount.Text = row;
            tb_colCount.Text = col;
        }
    }
}
