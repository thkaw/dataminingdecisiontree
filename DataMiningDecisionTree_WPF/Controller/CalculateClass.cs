﻿using DataMiningDecisionTree_WPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiningDecisionTree_WPF.Controller
{
    public class CalculateClass
    {
     
        public Double CountIteration(List<int> InputValue_List)
        {
            Double returnValue = 0;

            //分母
            Double Denominator = InputValue_List.Sum();

            foreach (int P in InputValue_List)
            {
                Double dP = P;
                Double Quotient = dP / Denominator;
                returnValue += -(Quotient * Math.Log(Quotient, 2));
            }

            return returnValue;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="OriginList"></param>
        /// <param name="HowManyAttribute"></param>
        /// <param name="SkipAttribute"></param>
        /// <param name="OverPercentToIgnore">請填入百分比小數，1代表不進行Attribute Value最佳化，0.3代表如果單一Attribute底下的Value超過OriginList總數量的30%則刪除該Set的Value忽略不計</param>
        /// <returns></returns>
        public List<List<String>> HowManyValuePerAttribute(List<List<string>> OriginList, int HowManyAttribute, List<AttributeModel> SkipAttribute, double OverPercentToIgnore)
        {

            List<List<string>> AttributeCatagory2D = new List<List<string>>();


            for (int z = 0; z < HowManyAttribute; z++)
            {
                AttributeCatagory2D.Add(new List<string>());
            }


            //var L = new object();
            //List<List<string>> resultCopy = new List<List<string>>();
            //bool hasNewMember = false;

            //  Parallel.For(0, OriginList.Count, i =>
            //{

            //    //橫向掃描
            //    for (int j = 0; j < HowManyAttribute; j++)
            //    {
            //        var NeedExclude = SkipAttribute.Where(x => x.Column.Equals(j));

            //        if (NeedExclude.Count() == 0)
            //        {

            //            lock (L)
            //            {
            //                if (hasNewMember)
            //                {
            //                    if (resultCopy[j].Count(p => p != null && p.Count() == AttributeCatagory2D[j].Count()) > 0) return;
            //                    hasNewMember = false;
            //                }

            //                var ExistItem = AttributeCatagory2D[j].Where(x => x.Contains(OriginList[i][j]));
            //                if (ExistItem.Count() == 0)
            //                {


            //                    AttributeCatagory2D[j].Add(OriginList[i][j]);
            //                    resultCopy = AttributeCatagory2D;
            //                    hasNewMember = true;
            //                }
            //            }


            //        }

            //    }

            //});



            for (int i = 0; i < OriginList.Count; i++)
            {
                //橫向掃描
                for (int j = 0; j < HowManyAttribute; j++)
                {
                    var NeedExclude = SkipAttribute.Where(x => x.Column.Equals(j));

                    if (NeedExclude.Count() == 0)
                    {
                        var ExistItem = AttributeCatagory2D[j].Where(x => x.Contains(OriginList[i][j]));
                        if (ExistItem.Count() == 0)
                        {
                            AttributeCatagory2D[j].Add(OriginList[i][j]);
                        }
                    }

                }

            }

            if (OverPercentToIgnore != 1)
            {
                foreach (var item in AttributeCatagory2D)
                {
                    //如果有一個attribute底下的value種類超過該set總量的OverPercentToIgnore，代表拿這個屬性來建tree的意義不大
                    if (item.Count() >= (int)OriginList.Count * OverPercentToIgnore)
                    {
                        //清空該屬性底下所有的value，代表忽略該屬性，在建tree時就不考慮
                        item.Clear();
                    }
                }
            }

            return AttributeCatagory2D;

        }

        /// <summary>
        /// 計算指定的Attribute的Value共有多少種，以及每一種共有多少個
        /// </summary>
        /// <param name="OriginList"></param>
        /// <param name="ClassesPosition"></param>
        /// <returns></returns>
        public List<AttributeStructure> HowManyAttributeValueCount(List<List<string>> OriginList, int ClassesPosition)
        {

            List<AttributeStructure> attStructures = new List<AttributeStructure>(10);
             
            foreach (string valueString in OriginList[ClassesPosition])
            {
                var ExistItem = attStructures.Where(x => x.ValueName.Contains(valueString));
                if (ExistItem.Count() > 0)
                {
                    //如果有了，可以處理一下該ItemSet的SupCount
                    ExistItem.First().Count++;
                }
                else
                {

                    //這個ItemSet不存在ItemList裡面
                    attStructures.Add(new AttributeStructure()
                    {
                        ValueName = valueString,
                        Count = 1
                    });

                }

            }

            return attStructures;

        }

        public class AttributeStructure
        {
            public string ValueName { get; set; }
            public int Count { get; set; }
        }

        public static List<List<T>> Transpose<T>(List<List<T>> lists)
        {
            var longest = lists.Any() ? lists.Max(l => l.Count) : 0;
            List<List<T>> outer = new List<List<T>>(longest);
            for (int i = 0; i < longest; i++)
                outer.Add(new List<T>(lists.Count));
            for (int j = 0; j < lists.Count; j++)
                for (int i = 0; i < longest; i++)
                    outer[i].Add(lists[j].Count > i ? lists[j][i] : default(T));
            return outer;
        }

    }

    public static class CollectionExtensions
    {

        private static Random random = new Random();

        public static IEnumerable<T> OrderRandomly<T>(this IEnumerable<T> collection)
        {

            // Order items randomly

            List<T> randomly = new List<T>(collection);

            while (randomly.Count > 0)
            {

                Int32 index = random.Next(randomly.Count);

                yield return randomly[index];

                randomly.RemoveAt(index);

            }

        } // OrderRandomly

    }

}
