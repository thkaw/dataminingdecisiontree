﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiningDecisionTree_WPF.Controller
{
    class ParseTXT
    {
        public async Task<List<List<string>>> Transform(string path)
        {

            List<List<string>> ListTrans2D = new List<List<string>>(50000);

            try
            {

                var lines = File.ReadLines(path);

                foreach (var line in lines)
                {
                    string[] ALine = line.Split('\t');

                    List<string> List_ALine = ALine.ToList();

                    ListTrans2D.Add(List_ALine);
                }
                return ListTrans2D;
                //using (StreamReader sr = new StreamReader(path))
                //{
                //    //   String s_AllContent = sr.ReadToEnd();

                //    String line = "";

                //    while ((line = sr.ReadLine()) != null)
                //    {
                //        string[] ALine = line.Split('\t');

                //        List<string> List_ALine = ALine.ToList();

                //        ListTrans2D.Add(List_ALine);


                //        // Console.WriteLine(line);

                //    }

                //    //  Console.WriteLine(ListTrans.Count);

                //    return ListTrans2D;
                //}



            }
            catch (Exception e)
            { throw; }
        }

        public DataTable TransformToDataTable(string path)
        {
            DataTable data = new DataTable(path);
            try
            {
                bool isFirst = true;
                var lines = File.ReadLines(path);

                foreach (var line in lines)
                {
                    string[] ALine = line.Split('\t');

                    if (isFirst)
                    {
                        isFirst = false;
                        foreach (string attributeName in ALine)
                        {
                            data.Columns.Add(attributeName, typeof(string));
                        }

                    }
                    else
                    { data.Rows.Add(ALine); }
                }


                return data;


            }
            catch (Exception e)
            { throw; }
        }
    }
}
