﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMiningDecisionTree_WPF.Model
{
    public class AttributeModel
    {
        /// <summary>
        /// 位於原始data的第幾個column
        /// </summary>
        public int Column { get; set; }
        public string AttributeName { get; set; }

        public List<string> AttributeValues { get; set; }

        /// <summary>
        /// 位於過濾後(運算處理期間)第幾個column(通常為該table最後一個)
        /// </summary>
        public int FilterColumn { get; set; }

    }

    public class AttributeValueCountModel
    {
        public string AttributeValueName { get; set; }
        public int Count { get; set; }


        /// <summary>
        /// Silver找成Silver
        /// </summary>
        public int TP_Count { get; set; }
        
        /// <summary>
        /// Other找成Silver
        /// </summary>
        public int FP_Count { get; set; }
        
        /// <summary>
        /// Silver找成Other
        /// </summary>
        public int FN_Count { get; set; }

    }

}
